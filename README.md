Rugs ‘N’ Rats has been providing high quality yet affordable Carpet Cleaning and Pest Control services to domestic and commercial clients across the Sunshine Coast for many years. We are local, reliable, and our work is 100% satisfaction guaranteed.

Address: 15 Magnetic St, Parrearra, QLD 4575, Australia

Phone: +61 1300 304 284

Website: https://rugsnrats.com.au
